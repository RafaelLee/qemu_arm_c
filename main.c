#include <stdio.h>
#include "stm32f407xx.h"
#include <core_cm4.h>

// int SystemInit()
// {
//     return 5+3;
// }

// Src/system_stm32f4xx.c
void SystemInit (void)
{
  /* FPU settings ------------------------------------------------------------*/
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
  SCB->CPACR |= ((3UL << 10 * 2) | (3UL << 11 * 2)); /* set CP10 and CP11 Full Access */
#endif
  /* Reset the RCC clock configuration to the default reset state ------------*/
  /* Set HSION bit */
  // RCC->CR |= (uint32_t)0x00000001;

  // /* Reset CFGR register */
  // RCC->CFGR = 0x00000000;

  // /* Reset HSEON, CSSON and PLLON bits */
  // RCC->CR &= (uint32_t)0xFEF6FFFF;

  // /* Reset PLLCFGR register */
  // RCC->PLLCFGR = 0x24003010;

  // /* Reset HSEBYP bit */
  // RCC->CR &= (uint32_t)0xFFFBFFFF;

  // /* Disable all interrupts */
  // RCC->CIR = 0x00000000;

  // #if defined (DATA_IN_ExtSRAM) || defined (DATA_IN_ExtSDRAM)
  //   SystemInit_ExtMemCtl();
  // #endif /* DATA_IN_ExtSRAM || DATA_IN_ExtSDRAM */

  /* Configure the Vector Table location add offset address ------------------*/
#ifdef VECT_TAB_SRAM
  // SCB->VTOR = SRAM_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal SRAM */
#else
  // SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */
  // 8 is from typedef struct{} SCB_Type; in
  volatile uint32_t *p = SCB + 8;
  *p = 0x8000000;
#endif
}



#define SystemCoreClock (8000000)


int main()
{
  // __HAL_RCC_WWDG_CLK_DISABLE();
  // #define __HAL_RCC_WWDG_CLK_DISABLE()   (RCC->APB1ENR &= ~(RCC_APB1ENR_WWDGEN))

  (RCC->APB1ENR &= ~ (RCC_APB1ENR_WWDGEN));

  NVIC_DisableIRQ (WWDG_IRQn);
  // NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* Use systick as time base source and configure 1ms tick (default clock after Reset is HSI) */

  // HAL_InitTick(TICK_INT_PRIORITY);
  // HAL_SYSTICK_Config(SystemCoreClock/1000U);
  SysTick_Config (SystemCoreClock / 1000U);

  /*Configure the SysTick IRQ priority */
  // HAL_NVIC_SetPriority(SysTick_IRQn, TICK_INT_PRIORITY, 0U);
  uint32_t prioritygroup = 0x00U;
  // prioritygroup = NVIC_GetPriorityGrouping();
  // NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, TICK_INT_PRIORITY, 0));
  NVIC_EnableIRQ (SysTick_IRQn);
  NVIC_DisableIRQ (WWDG_IRQn);

  while (1)
  {
    for (int i = 0; i < 100000000; i++) {}
    printf ("abc\n");
  }
}
